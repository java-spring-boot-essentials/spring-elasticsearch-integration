package com.samsaydali.elasticksearch;

import com.samsaydali.elasticksearch.models.Article;
import com.samsaydali.elasticksearch.models.Author;
import com.samsaydali.elasticksearch.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;


@RestController
@RequestMapping("/articles")
public class Controller {

    @Autowired
    ArticleRepository articleRepository;

    @GetMapping("/prepare")
    public Article prepare() {
        Article article = new Article("Spring Data Elasticsearch");
        article.setAuthors(Arrays.asList(new Author("John Smith"), new Author("John Doe")));
        articleRepository.save(article);


        Article article1 = new Article("Spring Data Elasticsearch 2");
        article1.setAuthors(Arrays.asList(new Author("Samer Alsaydali"), new Author("John Doe")));
        articleRepository.save(article1);

        return article;
    }

    @GetMapping("/search")
    public Page<Article> search(@RequestParam String name) {
        Page<Article> articleByAuthorName
                = articleRepository.findByAuthorsName(name, PageRequest.of(0, 10));

        return articleByAuthorName;
    }
}
