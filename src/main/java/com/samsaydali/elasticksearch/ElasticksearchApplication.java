package com.samsaydali.elasticksearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElasticksearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticksearchApplication.class, args);
    }

}
